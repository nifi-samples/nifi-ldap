# nifi-ldap

`Feel free to modify the processor as per your requirement`

We have implemented this processor according to our requirements.
Extend this one or modify it to suite your requirements.

### Custom nifi processor to fetch user information from ldap.

This processor connects to ldap and

1. Authenticate user against AD.
2. Check if the user has desired role.
3. Attach attribute "authorized=true" to flowfile.

![alt text](img.png)

`username and password` are nifi expressions and receive values from
upstream processor.

`Search base`, ldap url are configurations to connect to LDAP server.

User must have a role that is equal to `with role` in order to treat as valid and authorized user.

### Relations

![alt text](relation.png)
Valid and authorized users will be routed to `authorized` connections with `authorized=true` attribute set.

Unauthorized users will be routed to `unauthorized` connections with `authorized=false` attribute set.

`SSLSocketFactory and TrustManager`can be used to satisfy the certificate requirements. Extend it to validate the
certificates imposed by the organization.
I have kept it bypass those checks for simplicity.