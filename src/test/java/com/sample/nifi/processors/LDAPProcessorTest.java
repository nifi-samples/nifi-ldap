/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sample.nifi.processors;

import org.apache.commons.io.IOUtils;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class LDAPProcessorTest {

    /**
     * Test of onTrigger method, of class LDAPProcessor.
     */
    //@org.junit.Test
    public static void testOnTrigger() throws IOException {
        // Content to be mock a json file
        InputStream content = new ByteArrayInputStream("{\"hello\":\"nifi\"}".getBytes());
        // Generate a test runner to mock a processor in a flow
        TestRunner runner = TestRunners.newTestRunner(new LDAPProcessor());
        // Add properties
        runner.setProperty(LDAPProcessor.USERNAME, "");
        runner.setProperty(LDAPProcessor.PASSWORD, "");
        runner.setProperty(LDAPProcessor.LDAP_URL, "");
        runner.setProperty(LDAPProcessor.SEARCH_BASE, "");
        runner.setProperty(LDAPProcessor.ROLE_TO_SEARCH, "");
        // Add the content to the runner
        runner.enqueue(content);
        // Run the enqueued content, it also takes an int = number of contents queued
        runner.run(1);
        // All results were processed with out failure
        runner.assertQueueEmpty();
        // If you need to read or do additional tests on results you can access the content
        List<MockFlowFile> results = runner.getFlowFilesForRelationship(LDAPProcessor.AUTHORIZED);
        assertTrue("1 match", results.size() == 1);
        MockFlowFile result = results.get(0);
        String resultValue = new String(runner.getContentAsByteArray(result));
        System.out.println("Match: " + IOUtils.toString(runner.getContentAsByteArray(result)));
        // Test attributes and content
        result.assertAttributeEquals(LDAPProcessor.AUTHENTICATED, "true");
        result.assertContentEquals("true");
    }

}
