package com.sample.nifi.processors.ldap;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.Hashtable;

public class LDAPSRead {

    private static final String MEMBER_OF = "memberOf";
    private static final String GLOBAL = "global";
    private static final String XDS = "xds";

    public static boolean isValidUser(final String userName, final String password, final String searchBase, final String ldapUrl, String roleToSearch, boolean isXDS) {
        boolean isValidUser = false;
        final Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapUrl);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PROTOCOL, "ssl");
        env.put(Context.SECURITY_PRINCIPAL, GLOBAL + "\\" + userName);
        if (isXDS) {
            env.put(Context.SECURITY_PRINCIPAL, XDS + "\\" + userName);
        }
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.REFERRAL, "follow");
        env.put("java.naming.ldap.factory.socket", "com.sample.nifi.processors.ldap.MySSLSocketFactory");
        final DirContext dirContext;
        try {
            dirContext = new InitialDirContext(env);
            final String filter = "(&(objectclass=user)(sAMAccountName=" + userName + "))";
            final SearchControls constraints = new SearchControls();
            constraints.setSearchScope(2);
            final String[] attrIDs = {"cn"};
            constraints.setReturningAttributes(attrIDs);
            final String[] attributes = {MEMBER_OF};
            constraints.setReturningAttributes(attributes);
            final NamingEnumeration<SearchResult> e = dirContext.search(searchBase, filter, constraints);
            while (e.hasMore()) {
                final SearchResult entry = e.next();
                final Attribute attribute = entry.getAttributes().get(MEMBER_OF);
                for (int i = 0; i < attribute.size(); i++) {
                    final String role = (String) attribute.get(i);
                    isValidUser = role.contains(roleToSearch);
                    if (isValidUser) {
                        break;
                    }
                }
            }
        }
        catch (final Exception e) {
            e.printStackTrace();
        }
        return isValidUser;
    }

    public static void main(final String[] args) {
        //System.out.println(isValidUser());
    }

}
