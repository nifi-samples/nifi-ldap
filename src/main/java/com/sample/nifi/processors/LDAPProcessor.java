/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sample.nifi.processors;

import com.sample.nifi.processors.ldap.LDAPSRead;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@SideEffectFree
@Tags({"LDAP connection", "LDAP connection"})
@CapabilityDescription("Validate the user from AD against given role.")
public class LDAPProcessor extends AbstractProcessor {

    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;
    static final String AUTHENTICATED = "authenticated";
    static final PropertyDescriptor USERNAME = new PropertyDescriptor.Builder()
            .name("username")
            .required(true)
            .expressionLanguageSupported(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor PASSWORD = new PropertyDescriptor.Builder()
            .name("password")
            .required(true)
            .expressionLanguageSupported(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor SEARCH_BASE = new PropertyDescriptor.Builder()
            .name("search base")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor LDAP_URL = new PropertyDescriptor.Builder()
            .name("ldap url")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor ROLE_TO_SEARCH = new PropertyDescriptor.Builder()
            .name("with role")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    private static final PropertyDescriptor XDS = new PropertyDescriptor.Builder()
            .name("xds")
            .defaultValue("true")
            .required(false)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final Relationship AUTHORIZED = new Relationship.Builder()
            .name("authorized")
            .description("User is authorized")
            .build();
    private static final Relationship UNAUTHORIZED = new Relationship.Builder()
            .name("unauthorized")
            .description("User is  not authorized")
            .build();

    @Override
    public void init(final ProcessorInitializationContext context) {
        List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(USERNAME);
        properties.add(PASSWORD);
        properties.add(SEARCH_BASE);
        properties.add(LDAP_URL);
        properties.add(ROLE_TO_SEARCH);
        properties.add(XDS);
        this.properties = Collections.unmodifiableList(properties);
        Set<Relationship> relationships = new HashSet<>();
        relationships.add(AUTHORIZED);
        relationships.add(UNAUTHORIZED);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        AtomicBoolean validUser = new AtomicBoolean(false);
        final FlowFile flowfile = session.get();
        session.read(flowfile, in -> {
            try {
                String username = context.getProperty(USERNAME).evaluateAttributeExpressions(flowfile).getValue().trim();
                String password = context.getProperty(PASSWORD).evaluateAttributeExpressions(flowfile).getValue().trim();
                String ldapUrl = context.getProperty(LDAP_URL).getValue();
                String searchBase = context.getProperty(SEARCH_BASE).getValue();
                String role = context.getProperty(ROLE_TO_SEARCH).getValue();
                String xds = context.getProperty(XDS).getValue();
                validUser.set(LDAPSRead.isValidUser(username, password, searchBase, ldapUrl, role, Boolean.parseBoolean(xds)));
            }
            catch (Exception ex) {
                ex.printStackTrace();
                session.transfer(flowfile, UNAUTHORIZED);
            }
        });
        // Write the results to an attribute
        if (validUser.get()) {
            session.putAttribute(flowfile, AUTHENTICATED, String.valueOf(validUser.get()));
        }
        // To write the results back out to flow file
        session.write(flowfile, out -> out.write(String.valueOf(validUser.get()).getBytes()));
        if (validUser.get()) {
            session.transfer(flowfile, AUTHORIZED);
        } else {
            session.transfer(flowfile, UNAUTHORIZED);
        }
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return this.properties;
    }

}
